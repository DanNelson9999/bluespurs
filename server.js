const express = require('express');

const app = express();
const port = process.env.PORT || 5000;

const request = require('request');
const async = require('async');

// need to figuuire out why route is not accepting parameters
app.get('/product/search/', (req, res) => {
    console.log(req);

   let item = "ipad";

   // need to resolve promises
   let walmartItem = getLowestPricedItemWalmart(item);
   let bestBuy = getLowestPricedItemBestBuy(item);

   // Need to compare and creat object with store name, product name, and value
   console.log("Walmart items  *****************", walmartItem);
   console.log("BesttBuy items  *****************", bestBuy);
  res.send({data: 'IPad 9'});
});


// app.get('/product/search/:name', (req, res) => {
//     console.log(req);
//   res.send({ express: 'Hello From Express 2' });
// });

 
async function getLowestPricedItemWalmart(item) {
       let body = await request('http://api.walmartlabs.com/v1/search?query=ipod&format=json&apiKey=rm25tyum3p9jm9x9x7zxshfa', { json: true }, (err, res, body) => {
       if (err) { return console.log(err); }
    });
    
      let walmartItem;
        if(typeof body.items !== "undefined"){
        body.items.forEach(item => {
           
             if(walmartItem) {
                console.log("item ", item.salePrice)
                console.log("walmart ",  walmartItem.salePrice)
               if (walmartItem.salePrice > item.salePrice) {
                     walmartItem = item;
               }
             } else {
                walmartItem = item;
             }
         });
        }
    
     return walmartItem;
 }

 async function getLowestPricedItemBestBuy(item) {
        // Endpoint is not returning right products need to fix
        let body = await request('https://api.bestbuy.com/v1/products(search=ipad)?format=json&show=sku,name,salePrice&apiKey=pfe9fpy68yg28hvvma49sc89', { json: true }, (err, res, body) => {
            if (err) { return console.log(err); }
        });
     
           let bestBuyItem;
             if(typeof body.products !== "undefined"){
             body.products.forEach(item => {
                
                  if(bestBuyItem) {
                     console.log("item ", item.salePrice)
                     console.log("Best Buy ",  bestBuyItem.salePrice)
                    if (bestBuyItem.salePrice > item.salePrice) {
                        bestBuyItem = item;
                    }
                  } else {
                    bestBuyItem = item;
                  }
              });
             }
          
          return  bestBuyItem;

         
     }

app.listen(port, () => console.log(`Listening on port ${port}`));