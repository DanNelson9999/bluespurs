import React, { Component } from 'react';
import './App.css';
import Task1 from './components/Task1';

class App extends Component {
  constructor (props, context) {
    super()
    this.state = {
      response: null
    }
  }

  componentDidMount(){
    //  let data = {data: {task1: [3,4]}};
    //  this.setState(Object.assign({}, this.state, data));
     this.callApi()
      .then(res => this.setState({ response: res.data }))
      .catch(err => console.log(err));
     
  }

  callApi = async () => {
    const encodedValue = encodeURIComponent('ipad');
    const response = await fetch('/product/search/');
    const body = await response.json();


    if (response.status !== 200) throw Error(body.message);

    return body;
  };



  render() {
    console.log(this.state);
    return (
      <div className="App">
        <header className="App-header">
            <h1 className="App-title">Blue Spurs Code Test</h1>
        </header>
        <div className="App-Container">
            <Task1 data={this.state.response}/>
        </div>
      </div>
    );
  }
}

export default App;
