import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Task.css';


class Task1 extends Component {
  render() {
    return (
      <div className="taskTitle">
       <h1>{"Task 1"}</h1>
       <span>
         <span>{this.props.data}</span>
      </span>
      </div>
    );
  }
}

Task1.propTypes = {
  data: PropTypes.object,
}

export default Task1;